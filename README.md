VL670 Development Board Firmware - USB 2.0 to 3.0 Transaction Translator
========================================================================

This is the firmware files of the VL670 and VL671 ASIC.

There are great differences between both firmwares files, the VL670 firmware
is likely an Xtensa executable, while the VL671 firmware is a ARM Cortex M3
executable. They're not interchangable, make sure the firmware matches your
chip.

This is a component of the VL670 development board, which is an effort to
create a free and open source hardware design of a development board for
the VL670/VL671 ASIC, to facilitate hardware evaluation and experiments
by other community developers.

This development board is intended solely for hardware or software developers
for use in a research and development setting to facilitate evaluation,
experimentation, or technical analysis. It's not designed for consumer use,
and it also should not be used as a part or subassembly in any finished product.

The main repository is:

* https://notabug.org/niconiconi/vl670/

### License

This is a proprietary firmware by VIA Labs. It should only be used for the
VL670 ASIC.
